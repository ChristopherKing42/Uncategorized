from opensimplex import OpenSimplex
from PIL import Image
import random
from heapq import *
import math

def noiseMaker():
	smplxs = []
	amp = 0
	for length in range(1,21):
		smplxs.append((1.0/length, OpenSimplex(seed = random.getrandbits(32))))
		amp += length
	def noise((x,y)):
		value = 0
		for (freq, smplx) in smplxs:
			a = math.sin(2*math.pi*x/128)*24*freq
			b = math.cos(2*math.pi*x/128)*24*freq
			c = math.sin(2*math.pi*y/256)*48*freq
			d = math.cos(2*math.pi*y/256)*48*freq
			value += smplx.noise4d(a,b,c,d)/freq
		return value/amp
	
	values = []
	for x in range(128):
		for y in range(256):
			heappush(values, (noise((x,y)), (x,y)))

	newValues = {}
	for i in range(128*256):
			(_, (x,y)) = heappop(values)
			newValues[(x,y)] = i/(128.0*256.0)
	
	return newValues

elevation = noiseMaker()
trees = noiseMaker()

cities = []
for i in range(10):
	while True:
		city = (random.randrange(128), random.randrange(256))
		if elevation[city] < 0.2: continue
		cities.append(city)
		break

def road(city1, city2):
	heap = [((abs(city1[0]-city2[0])+abs(city1[1]-city2[1]))/5.0, 0, [city1])]
	visited = set()
	while True:
		if heap == []: return []
		(_, cost, partRoad) = heappop(heap)
		point = partRoad[0]
		for (dx, dy) in [(0,1), (0,-1), (1, 0), (-1, 0)]:
			newPoint = ((point[0]+dx)%128, (point[1]+dy)%256)
			if newPoint == city2: return partRoad
			if elevation[newPoint] < 0.2: continue
			if newPoint in visited: continue
			visited.add(newPoint)
			newCost = 1 + 10**2 * abs(elevation[newPoint] - elevation[point])**0.5
			if newPoint in roads: newCost /= 5.0
			newEstimate = newCost + (abs(newPoint[0] - city2[0]) + abs(newPoint[1] - city2[1]))/5.0
			newPartRoad = [newPoint] + partRoad
			heappush(heap, (newEstimate, newCost, newPartRoad))

roads = []
for city1 in cities:
	while True:
		city2 = random.choice(cities)
		if city2 == city1: continue
		else: break
	roads.extend(road(city1, city2))

im = Image.new("RGB", (256,128))
pixels = []
for x in range(128):
	for y in range(256):
		if (x,y) in cities:
			pixels.append((256,0,0))
		elif (x,y) in roads:
			pixels.append((128,128,256))
		elif elevation[(x,y)] >= 0.2:
			treeProb = trees[(x,y)]
			treeProb *= 0.3
			if random.random() < treeProb:
				pixels.append((0,256,0))
			else:
				value = int((elevation[(x,y)] - 0.2) / .8 * 256)
				pixels.append((value,value, value))
		else:
			pixels.append((0,0,256))

im.putdata(pixels)
im.save("map.bmp")
