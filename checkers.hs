import Data.Word

data Side = B | W deriving (Show, Eq, Ord)
data Piece = Man !Side | King !Side deriving (Show, Eq, Ord)
data Space = Empty | Piece !Piece deriving (Show, Eq, Ord)

type Coord = (Word8, Word8)
type Pos = Coord -> Space

coords :: [Coord]
coords = [(x,y) | x <- [0..7], y <- [0..7], (x+y) `mod` 2 == 1]

start :: Pos
start (x, y) | (x + y) `mod` 2 == 0 = error $ show (x,y) ++ " is not a valid space."
             | x < 0 || x > 7 || y < 0 || y > 7 = error $ show (x,y) ++ " is not a valid space."
             | x <= 2 = Piece $ Man B
             | 5 <= x = Piece $ Man W
             | otherwise = Empty

showPos :: Pos -> String
showPos pos = unlines [[char x y | y <- [0..8]] | x <- [0..8]] where
    char 8 8 = ' '
    char 8 y = ['A'..'H'] !! (fromIntegral y)
    char x 8 = ['1'..'8'] !! (fromIntegral x)
    char x y | (x + y) `mod` 2 == 0 = ' '
             | otherwise = case pos (x, y) of
                Empty -> '+'
                Piece (Man B) -> 'b'
                Piece (Man W) -> 'w'
                Piece (King B) -> 'B'
                Piece (King W) -> 'W'

update :: Pos -> [(Coord, Space)] -> Pos
update pos table coord = case lookup coord table of
    Just space -> space
    Nothing -> pos coord

moves :: Pos -> Side -> [Pos]
moves pos side = poss where
    poss = coords >>= getPoss
    getPoss coord = case pos coord of
        Piece (Man B) -> move B False coord
        Piece (King B) -> move B True coord
        Piece (Man W) -> move W False coord
        Piece (King W) -> move W True coord
        Empty -> []
    
    move side' _ _ | side /= side' = []
    move _ king coord@(x,y) = do
        coord'@(x', y') <- if king then [(x-1, y-1), (x-1, y+1), (x+1, y-1), (x+1, y+1)] else case side of
            B -> [(x+1, y-1), (x+1, y+1)]
            W -> [(x-1, y-1), (x-1, y+1)]
        if x' < 0 || x' > 7 || y' < 0 || y' > 7
            then []
            else return ()
        case pos coord' of
            Empty -> return $ update pos [(coord, Empty), (coord', Piece $ if king then King side else Man side)]
            _ -> []
