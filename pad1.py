from opensimplex import OpenSimplex
from PIL import Image
import random
from heapq import *
import math

def noiseMaker():
	smplxs = []
	amp = 0
	for length in [2,4,8,16,32]:
		smplxs.append((1.0/length, OpenSimplex(seed = random.getrandbits(32))))
		amp += length**2
	def noise((x,y)):
		value = 0
		for (freq, smplx) in smplxs:
			value += smplx.noise2d(x*freq, y*freq)/freq**2
		return value/amp
	
	values = []
	for x in range(128):
		for y in range(128):
			heappush(values, (noise((x,y)), (x,y)))

	newValues = {}
	for i in range(128*128):
			(value, point) = heappop(values)
			newValues[point] = (value + 1)/2
	
	return newValues
elevation = noiseMaker()

im = Image.new("RGB", (128,128))
pixels = []
for x in range(128):
	for y in range(128):
		value = int(elevation[(x,y)]*128)
		pixels.append((value,value, value))

im.putdata(pixels)
im.save("model.bmp")
