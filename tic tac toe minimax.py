#Fix erroring at the end during a tie and fix passing algorithm
import minimax

WAYS_TO_WIN = ((0, 1, 2),
               (3, 4, 5),
               (6, 7, 8),
               (0, 3, 6),
               (1, 4, 7),
               (2, 5, 8),
               (0, 4, 8),
               (2, 4, 6))

def get_winner(board):
    for row in WAYS_TO_WIN:
        if board[row[0]] == board[row[1]] == board[row[2]] != " ":
            winner = board[row[0]]
            return winner
    if " " not in board:
        return "TIE"
    else:
        return None #If no winner, return none

def itr_moves(hyp_board, parent, depth, turn, human):
    new_node=minimax.node(hyp_board)
    minimax.add_node(new_node, parent, depth)
    winner = get_winner(hyp_board)
    if not winner:
        for x in xrange(len(hyp_board)): #For every space in the board
            if hyp_board[x] == " ":
                new_hyp_board=hyp_board[:]
                if turn: #If it's the human's hypothetical turn
                    new_hyp_board[x]="X"
                else:
                    new_hyp_board[x]="O"
                itr_moves(new_hyp_board, new_node, depth+1, not turn, human)
    else:
        if winner != "TIE":
            if human: #If the human went first
                if winner == "X":
                    val=-1 #The value of the board
                else:
                    val=1
            else:
                if winner == "X":
                    val=1
                else:
                    val=-1
            new_node.value=val

def get_cpu_move(board):
    
    greatest=max(minimax.tree[1], key=lambda x:x.value)
    
    for x in xrange(len(greatest.state)):
        if greatest.state[x] != board[x]:
            return x

def display_board(board):
    print "\n\t", board[0], "|", board[1], "|", board[2]
    print "\t", "---------"
    print "\t", board[3], "|", board[4], "|", board[5]
    print "\t", "---------"
    print "\t", board[6], "|", board[7], "|", board[8], "\n"

def get_move(board):
    while True:
        move=int(raw_input("Input move: "))
        if board[move] == " ":
            return move
        else:
            print "Disallowed move!"
    
def main():
    
    board=[" "," "," "," "," "," "," "," "," "]

    if raw_input("Would you like to go first (y/n)? ") == "y":
        human=True
    else:
        human=False
    while not get_winner(board):
        if human:
            minimax.tree=[]
            display_board(board)
            
            board[get_move(board)]="X"
            if get_winner(board):
                break
            itr_moves(board, None, 0, False, human)
            minimax.pass_values(minimax.tree[0][0], False,2)
            board[get_cpu_move(board)]="O"
        else:
            minimax.tree=[]
            itr_moves(board, None, 0, True, human)
            minimax.pass_values(minimax.tree[0][0], False,2)
            board[get_cpu_move(board)]="X"
            display_board(board)
            if get_winner(board):
                break
            board[get_move(board)]="O"
    print "GAME ENDED"
    print "WINNER IS: " + get_winner(board)
    raw_input()                         
            
main()
