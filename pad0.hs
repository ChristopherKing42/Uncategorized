import Codec.Picture
import Data.Complex

angleA = pi/3
angleB = pi/8

sideA = acosh $ cos angleA / sin angleB
sideB = acosh $ cos angleB / sin angleA

poincareA = (tanh $ sideA / 2, 0)
poincareB = (0, tanh $ sideB / 2)

line (ax, ay) (bx, by) = if den == 0 then Left line else Right circle where
    den = ax*by - bx*ay
    
    line = (ay - by, bx - ax, den)

    s1 = (1 + ax^2 + ay^2)/2
    s2 = (1 + bx^2 + by^2)/2
    center = ((s1*by - s2*ay) / den, (ax*s2 - bx*s1)/den)
    radius = sqrt $ fst center ^2 + snd center ^2 - 1
    circle = (center, radius)

Right hyp = line poincareA poincareB

circleToBand (x,y) = let x' :+ y' = 4 / pi * atanh (x :+ y) in (x', y')

dist2 (x1,y1) (x2,y2) = (x1-x2)^2 + (y1-y2)^2

close line pt@(x,y) = igo where
    error = case line of
        Left (a, b, c) -> abs (a*x + b*y + c) / sqrt (dist2 (a,b) (0,0))
        Right (c, r) -> abs $ sqrt (dist2 c pt) - r
    
    igo = error < 0.01 / diskMet
    diskMet = 2 / (1 - dist2 pt (0,0))
    bandMet = 1 / (cos $ pi/2 * by)
    (bx, by) = circleToBand pt

theLines = map (\(x1,y1,x2,y2) -> line (bandToCircle (x1,y1)) (bandToCircle (x2,y2)))
    [(1,0, -1,0), (-1,1, -1,-1), (1,1, 100,0), (-1.5,-1, -2.5,-1), (0,-1, 0.5, 1), (-2,1, 2,1), (-2.5,1, 1.5,-1)]

data Color = One | Two | Three deriving (Eq, Show)

xflip One = Two
xflip Two = One
xflip Three = Three

hflip Two = Three
hflip Three = Two
hflip One = One

circleToColor pt@(x,y)
    | x < 0 = xflip $ circleToColor (-x, y)
    | y < 0 = circleToColor (x, -y)
    | dist2 pt (fst hyp) < snd hyp ^2 = hflip $ circleToColor (fst (fst hyp) + snd hyp^2 * (x - fst (fst hyp)) / dist2 pt (fst hyp), snd (fst hyp) + snd hyp^2 * (y - snd (fst hyp)) / dist2 pt (fst hyp))
    | otherwise = One

red = PixelRGB8 255 0 0
white = PixelRGB8 255 255 255
blue = PixelRGB8 0 0 255

circleToPixel pt = case circleToColor pt of
    One -> red
    Two -> white
    Three -> blue

bandToCircle (x,y) = let x' :+ y' = tanh $ (x :+ y) * pi/4 in (x', y')

rot phi (x,y) = (x*cos phi - y*sin phi, x*sin phi + y*cos phi)

xc = (-3,3)
yc = (-1,1)

dimx = 3000
dimy = 1000

normalize (x,y) = ((fromIntegral x/fromIntegral dimx)*(snd xc - fst xc) + fst xc,
                   (fromIntegral y/fromIntegral dimy)*(snd yc - fst yc) + fst yc)

image = generateImage (\x y -> circleToPixel $ rot (pi/2) $ bandToCircle $ normalize (x,y)) dimx dimy

main = writePng "band.png" image
