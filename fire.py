from opensimplex import OpenSimplex
import numpy as np
from array2gif import write_gif
import random
from heapq import *
import math

def noiseMaker():
	smplxs = []
	amp = 0
	for length in [3,5,10,20]:
		smplxs.append((1.0/length, OpenSimplex(seed = random.getrandbits(32))))
		amp += length
	def noise((x,y,z)):
		value = 0
		for (freq, smplx) in smplxs:
			a = x * freq
			b = y * freq
			c = math.sin(2*math.pi*z/30)/(2*math.pi)*freq*100
			d = math.cos(2*math.pi*z/30)/(2*math.pi)*freq*100
			value += smplx.noise4d(a,b,c,d)/freq
		return value/amp
	
	values = []
	for z in range(30):
		for x in range(128):
			for y in range(256):
				heappush(values, (noise((x,y,z)), (x,y,z)))

	newValues = {}
	for i in range(128*256*30):
			(_, point) = heappop(values)
			newValues[point] = i/(128.0*256.0*30.0)
	
	return newValues

noise = noiseMaker()

frames = []
for z in range(30):
	frame = []
	for x in range(128):
		column = []
		for y in range(128):
			temp = (noise[x,y,z] + y/128.0)/2
			if temp < 0.5:
				column.append((0,0,0))
			elif temp < 0.65:
				column.append((255,255,0))
			elif temp < 0.8:
				column.append((255,128,0))
			else:
				column.append((255,0,0))
		frame.append(column)
	frame = np.array(frame)
	frames.append(frame)
			
dataset = frames

write_gif(dataset, 'fire.gif', fps=30)
