from lxml import html
from multiprocessing.pool import ThreadPool
import requests

def limerick(i):
    try:
        li = requests.get('https://www.oedilf.com/db/Lim.php?VerseId=A' + str(i))
    except requests.exceptions.ConnectionError:
        print "Limerick %d failed." % i
        return ''
    tree = html.fromstring(li.content)
    limerick = tree.xpath('//div[@id="content"]//div[@class="limerickverse"]//text()')
    limerick = ''.join(limerick)
    return limerick

limericks = ThreadPool(50).imap_unordered(limerick, xrange(1,110000))
i = 0
f = open("limericks.txt", "w")
try:
    for limerick in limericks:
        print i
        i += 1
        if limerick == '': continue
        f.write(limerick.encode('utf-8'))
        f.write('\n\n')
finally:
    f.close()
