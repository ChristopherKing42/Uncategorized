class Var(object):
    def __init__(self, n, name = None):
        self.index = n
        self.name = name
    def __repr__(self):
        if self.name: return str(self.index) + ':' + self.name
        else: return str(self.index)

class Lam(object):
    def __init__(self, body, name = None):
        self.body = body
        self.name = name
    def __repr__(self):
        if self.name: return self.name
        else: return "\." + str(self.body)

class App(object):
    def __init__(self, func, arg, name = None):
        self.func = func
        self.arg = arg
        self.name = name
    def __repr__(self):
        if self.name: return self.name
        else:
            sFunc = str(self.func)
            sArg = str(self.arg)
            
            if type(self.func) == Lam:
                sFunc = "(" + sFunc + ")"
            if type(self.arg) in [Lam, App]:
                sArg = "(" + sArg + ")"
            
            if sFunc[-1].isdigit() and sArg[0].isdigit() :
                return sFunc + ' ' + sArg
            else:
                return sFunc + sArg

def lams(n, term):
    for i in xrange(n):
        term = Lam(term)
    return term
def apps(func, *args):
    for arg in args:
        func = App(func, arg)
    return func

I = Lam(Var(0), 'I')
K = lams(2, Var(1))
S = lams(3, App(App(Var(2), Var(0)), App(Var(1), Var(0))))
omega = Lam(App(Var(0), Var(0)))
Omega = App(omega, omega)
Y = Lam(App(Lam(App(Var(1), App(Var(0), Var(0)))), Lam(App(Var(1), App(Var(0), Var(0))))), 'Y')

def church(n):
    body = Var(0)
    for i in xrange(n):
        body = App(Var(1), body)
    term = lams(2, body)
    term.name = '#' + str(n)
    return term

isZero = lams(3, apps(Var(2), Lam(Var(1)), Var(1)))
isZero.name = 'isZero'
Pred = lams(3, apps(Var(2), lams(2, App(Var(0), App(Var(1), Var(3)))), Lam(Var(1)), I))
Pred.name = 'Pred'

factF = lams(2, apps(isZero, Var(0), I, lams(2, apps(Var(3), App(Pred, Var(2)), App(Var(2), Var(1)), Var(0)))))
factF.name = 'factF'
fact = App(Y, factF, 'fact')

def betaD(body, arg, depth=0):
    if type(body) == Var:
        if body.index < depth:
            return (body, False)
        elif body.index == depth:
            return (shift(depth, arg), True)
        elif body.index > depth:
            return (Var(body.index - 1, body.name), False)
    elif type(body) == Lam:
        (term, changed) = betaD(body.body, arg, depth+1)
        if changed:
            return (Lam(term), True)
        else:
            return (Lam(term, body.name), False)
    elif type(body) == App:
        (func, funcChanged) = betaD(body.func, arg, depth)
        (arg, argChanged) = betaD(body.arg, arg, depth)
        if funcChanged or argChanged:
            return (App(func, arg), True)
        else:
            return (App(func, arg, body.name), False)
    else: raise TypeError

def beta(body, arg):
    return betaD(body, arg)[0]

def shift(n, term, depth=0):
    if type(term) == Var:
        if term.index < depth:
            return term
        elif term.index + n < depth:
            raise ValueError, "If you shift a negative amount, you may not cause free variables to become bound."
        else:
            return Var(term.index + n, term.name)
    elif type(term) == Lam:
        return Lam(shift(n, term.body, depth+1), term.name)
    elif type(term) == App:
        return App(shift(n, term.func, depth), shift(n, term.arg, depth), term.name)
    else: raise TypeError

def normalForm(term, limit = float('inf'), head = False):
    original = term
    steps = 0
    lambdas = 0
    revArgs = []
    while steps < limit:
        if type(term) == Var:
            for arg in reversed(revArgs):
                if not head:
                    (arg, stepsArg) = normalForm(arg, limit - steps, False)
                    steps += stepsArg
                term = App(term, arg)
            if not head:
                count = countFree(term)
                i = 0
                while (
                    steps < limit and
                    lambdas > 0 and
                    type(term) == App and
                    type(term.arg) == Var and
                    term.arg.index == i and
                    count[i] == 1):
                        term = term.func
                        steps += 1
                        lambdas -= 1
                        i += 1
                if i != 0: term = shift(-i, term)
            if steps == 0:
                return (original, 0)
            return (lams(lambdas, term), steps)
        elif type(term) == Lam:
            if revArgs:
                arg = revArgs.pop()
                term_ = beta(term.body, arg)
                term = term_
                steps += 1
            else:
                term = term.body
                lambdas += 1
        elif type(term) == App:
            revArgs.append(term.arg)
            term = term.func
        else:
            raise TypeError

    return (lams(lambdas, apps(term, *reversed(revArgs))), steps)

def inc(d, k):
    if k in d: d[k] += 1
    else: d[k] = 1

def countFree(term, depth=0, count = None):
    if count == None:
        count = {}
    if type(term) == Var:
        if term.index >= depth: inc(count, term.index - depth)
    elif type(term) == Lam:
        countFree(term.body, depth+1, count)
    elif type(term) == App:
        countFree(term.func, depth, count)
        countFree(term.arg, depth, count)
    return count
