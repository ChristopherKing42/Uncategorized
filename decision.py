import json
def mal(): print "Sorry, the input was malformed. Try again."
def rep(u): return repr(str(u))

print "BEGIN PROGRAM\n"
print "Hello, today I'm going to help you make a decision. This program assumes that there is some utility function you are trying to maximize. If you are VNM-rational, you can use utils.py to generate one. If you already have one, you can use that instead.\n"
print "Please enter the filename of the file that contains your utility function."
while True:
    filename = raw_input("FILENAME: ")
    try:
        with open(filename) as f: utils = json.load(f)
        for v in utils.values():
            assert type(v) == float
    except IOError: print "That file does not exist.",
    except (ValueError, TypeError, AssertionError): print "That file does not contain a well formed utility function.",
    else: break
    print "Please try again."
print

print 'Now enter the what you need to decide. (If you leave the input blank, it defaults to "What pizza to order".)'
scenario = raw_input("THING TO DECIDE: ")
print
if scenario == '': scenario = "What pizza to order"

print 'Now enter what options you have. The options should be describe precisely enough that you must choose one within a finite amount of time. For example, "Buy a car eventually" would not be a good option. "Buy a car by Tuesday" would be a good option. (Enter the empty string when you are done.)\n'
options = []
nOpt = 0
while True:
    option = raw_input("OPTION #{nOpt}: ".format(nOpt=nOpt))
    if not option:
        if nOpt == 0:
            print "You must input at least one option."
            continue
        else: break
    options.append(option)
    nOpt += 1
print

while True:
    complete = raw_input("Is it physically possible to *not* choice one of those options (by choosing some other option or doing nothing, for instance)?(y/n) ")
    complete = complete[0].lower()
    if complete == 'y':
        options.append("Other/Do nothing")
        nOpt += 1
        break
    elif complete == 'n': break
    else: mal()
print

print "Reminder: These are the following possible outcomes of your life."
for outcome in utils.keys():
    print "\t" + outcome
print
print "When I ask you for a set of probabilities, enter a natural number for each event. The program will scale them (as floats) so their total is one. This is similar to the notion of odds.\n"

def askProb(q):
    while True:
        prob = {}
        for outcome in utils.keys():
            try:
                p = int(raw_input(q(outcome)))
            except ValueError: mal()
            else:
                if p < 0: print "Probabilites can not be negative. Please try again."
                else:
                    prob[outcome] = p
                    continue
            break
        else: break
    return prob

startProb = askProb(lambda o: 'Currently, what is the probability of {outcome}? '.format(outcome = rep(o)))
startUtil = 0.0
norm = 0
for (outcome, util) in utils.items():
    startUtil += startProb[outcome]*util
    norm += startProb[outcome]
startUtil /= float(norm)

probs = {}
for option in options:
    print "Consider the scenario in which you choose {option}. What is the probability of each outcome?".format(option=rep(option))
    probs[option] = askProb(lambda o: '\t{outcome} (currently it is {chance} out of {norm}): '.format(outcome = rep(o), chance=startProb[o], norm=norm))

print "Good, now we can calculate the change in utility each option will bring.\n"
afterUtils = {}
for option in options:
    afterUtils[option] = 0.0
    norm = 0
    for (outcome, util) in utils.items():
        afterUtils[option] += probs[option][outcome]*util
        norm += probs[option][outcome]
    afterUtils[option] /= float(norm)
    print "If you choose {option}, then /\\E[u_{{{filename}}}(outcome)] = {change} utils.".format(option=rep(option), filename=filename, change=afterUtils[option]-startUtil)
print

bestUtil=max(afterUtils.values())
bests = [outcome for (outcome, util) in afterUtils.items() if util == bestUtil]
bestStr = '{' + ', '.join(map(rep, bests)) + '}'
print "Therefore, the best option for {scenario} is one of {bestStr}.\n".format(scenario=rep(scenario), bestStr=bestStr)
print """If you do not choose one of {bestStr}, then at least one of the following is true:
\tYou choose irrationally.
\tu_{{{filename}}} does not reflect your actual preferences. This could be due to a lack of precision. (If you used utils.py to generate it, either you answered the prompts incorrectly, you are not VNM-rational, or utils.py's precise was not enough for this decision.)
\tThe domain of u_{{{filename}}} does not include all possible outcomes for your life that this choice would influence.
\tThe domain of u_{{{filename}}} contained outcomes that were vague or not specific enough. (For example, they may not have taken into account your life as a whole.)
\tYou did not input all possible options for {scenario} (or more became avaible), and the option you actually chose was not inputted.
\tEach option in {bestStr} was not actually an option (or became unavailable). For example, something physical or psychological may have stopped you from choosing one of them.
\tThe probabilities you gave for the hypothetical situations did not reflect your actual beliefs at the time of the choice. Perhaps you forgot to take into account how the costs of the options influenced the probabilities. Or perhaps you received information that changed the probabilities.\n""".format(bestStr=bestStr, filename = filename, scenario=rep(scenario))

print 'In any case, have a nice day.\n'
print 'END PROGRAM.'
