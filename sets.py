from collections import Iterator
from functools import partial

class _OppOrder(object):
    '''Given an object, returns one with order operations reversed.'''
    def __init__(self, obj): self._robj = obj

    def __lt__(self, other): return other._robj <  self._robj
    def __le__(self, other): return other._robj <= self._robj
    def __eq__(self, other): return other._robj == self._robj
    def __ne__(self, other): return other._robj != self._robj
    def __gt__(self, other): return other._robj >  self._robj
    def __ge__(self, other): return other._robj >= self._robj
    def __cmp__(self, other): return cmp(other._robj, self._robj)
    
    def __repr__(self): return "oppOrder({_robj})".format(_robj = repr(self._robj))
    def __str__(self): return "oppOrder({_robj})".format(_robj = str(self._robj))

class _Communism(object):
    '''A class in which all objects are equal.'''
    def __init__(self, obj): self._cobj = obj
    def __cmp__(self, other): return 0

    def __repr__(self): return "oppOrder({_robj})".format(_robj = repr(self._robj))
    def __str__(self): return "oppOrder({_robj})".format(_robj = str(self._robj))

class Set(object):
    def __init__(self, maximum):
        '''There should exist some set such that `maximum(key)` finds the maximum of `key`, for any function `key`, over elements of the set. The set should be one for which a maximum exists for each `key`. In particular, this implies is must be nonempty.'''
        self._maximum = maximum
    
    def maximum(self, key): return self._maximum(key)
    def minimum(self, key):
        res = self._maximum(lambda el: _OppOrder(key(el)))
        return res._robj
    
    def maxPair(self, key):
        val, el = self._maximum(lambda el: (key(el), _Communism(el)))
        return (el._cobj, val)
    def minPair(self, key):
        val, el = self._maximum(lambda el: (_OppOrder(key(el)), _Communism(el)))
        return (el._cobj, val._robj)

    def maximize(self, key): return self.maxPair(key)[0]
    def minimize(self, key): return self.minPair(key)[0]

    def epsilon(self, pred, boolean=False):
        '''Find an element of the set `el` such that `pred(el)` iff exists `el_`. `pred(el)`. If `pred` might output a non-bool, set boolean to `False`. Otherwise, you may set it to `True`.'''
        if boolean: pred_ = pred
        else: pred_ = lambda el: bool(pred(el))
        return self.maximize(pred_)

    def find(self, pred, boolean=False):
        '''Will throw `ValueError` if no element satisfying `pred` exists.'''
        if boolean: pred_ = pred
        else: pred_ = lambda el: bool(pred(el))
        (el, exists) = self.maxPair(pred_)
        if exists: return el
        else: raise ValueError, "Not found."

    def exists(self, pred, boolean=False):
        if boolean: pred_ = pred
        else: pred_ = lambda el: bool(pred(el))
        return self.maximum(pred_)
    def forall(self, pred): #boolean is not necessary since we need to negate anyways
        return not self.maximum(lambda el: not pred(el))

    def __contains__(self, el): return self.exists(lambda el_: el_ == el, True)
    def __eq__(self, other): return subseteq(self, other) and subseteq(other, self)
    def max(self): return self.maximum(lambda el: el)
    def min(self): return self.minimum(lambda el: el)
    def example(self):
        '''Return an arbitrary element of the set.'''
        return self.maximize(lambda el: ())

def subseteq(s1, s2): return s1.forall(lambda el: el in s2)

def fromCont(container):
    if isinstance(container, Iterator): raise TypeError, "Do not use an iterator. Use some other type of iterable instead."
    try: next(iter(container))
    except StopIteration: raise ValueError, "Sets must be nonempty."

    else: return Set(lambda key: max(key(el) for el in container))

def fromArgs(*args): return fromCont(args)

def product(*args):
    def maximum(key):
        def fold(args, acc):
            if args: return args[0].maximum(lambda x: fold(args[1:], acc + (x,)))
            else: return key(acc)
        return fold(args, ())
    return Set(maximum)

def fromFunc(fSet):
    '''Given some function `fSet` whose values are sets, return a set whose elements are functions. `fromFunc(fSet)` consists of functions `f` such that `f(x) in fSet(x)` for all `x`. (There is actually one more requirement. For `f(x)` to be defined, `x` must be able to be a key in a dictionary.)'''
    def maximumF(key):
        def guesser(memo, x):
            if x in memo: return memo[x]
            else:
                def score(el):
                    memo_ = memo.copy()
                    memo_[x] = el
                    try: return key(partial(guesser, memo_))
                    finally: del memo_
                el = fSet(x).maximize(score)
                memo[x] = el
                return el

        memo = {}
        def maximizedF(x): return guesser(memo, x)
        return key(maximizedF)

    return Set(maximumF)

def one(el): return Set(lambda key: key(a))
def bind(s, f):
    '''`el` is an element of `bind(s, f)` iff there exists `a in s` such that `el in f(a)`. In otherwords, `f` maps elements of `s` to subsets of `bind(s,f)`.'''
    return Set(lambda key: s.maximum(lambda a: f(a).maximum(key)))

def union(*args):
    assert args
    return Set(lambda key: max(s.maximum(key) for s in args))
def setUnion(ss): return bind(ss, lambda s: s)
def fmap(f, s):
    '''Applies `f` to each member of `s` and returns the resulting set.'''
    return Set(lambda key: s.maximum(lambda a: key(f(a))))

def filterSet(s, pred, boolean = False):
    if not s.exists(pred, boolean): raise ValueError, "That filteration would result in the empty set."
    return Set(lambda key:
            s.maximum(lambda el:
                (True, key(el)) if p(el) else (False, None))[1])

def intersect(*args):
    assert args
    return filterSet(args[0], lambda el: all(el in s for s in args[1:]))
def setIntersect(ss): return filterSet(ss.example(), lambda el: ss.forall(lambda s: el in s))
def diff(s1, s2): return filterSet(s1, lambda el: el not in s2)
