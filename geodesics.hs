import Codec.Picture
import Data.Complex

line (ax, ay) (bx, by) = if den == 0 then Left line else Right circle where
    den = ax*by - bx*ay
    
    line = (ay - by, bx - ax, den)

    s1 = (1 + ax^2 + ay^2)/2
    s2 = (1 + bx^2 + by^2)/2
    center = ((s1*by - s2*ay) / den, (ax*s2 - bx*s1)/den)
    radius = sqrt $ fst center ^2 + snd center ^2 - 1
    circle = (center, radius)

circleToBand (x,y) = let x' :+ y' = 4 / pi * atanh (x :+ y) in (x', y')

dist2 (x1,y1) (x2,y2) = (x1-x2)^2 + (y1-y2)^2

close line pt@(x,y) = igo where
    error = case line of
        Left (a, b, c) -> abs (a*x + b*y + c) / sqrt (dist2 (a,b) (0,0))
        Right (c, r) -> abs $ sqrt (dist2 c pt) - r
    
    igo = error < 0.004 / diskMet * bandMet
    diskMet = 2 / (1 - dist2 pt (0,0))
    bandMet = 1 / (cos $ pi/2 * by)
    (bx, by) = circleToBand pt

red = PixelRGB8 255 0 0
white = PixelRGB8 255 255 255

theLines = map (\(x1,y1,x2,y2) -> line (bandToCircle (x1,y1)) (bandToCircle (x2,y2)))
    [(1,0, -1,0), (-1,1, -1,-1), (1,1, 100,0), (-1.5,-1, -2.5,-1), (0,-1, 0.5, 1), (-2,1, 2,1), (-2.5,1, 1.5,-1)]

circleToPixel pt
    | any (\line -> close line pt) theLines = red
    | otherwise = white


bandToCircle (x,y) = let x' :+ y' = tanh $ (x :+ y) * pi/4 in (x', y')

xc = (-3,3)
yc = (-1,1)

dimx = 3000
dimy = 1000

normalize (x,y) = ((fromIntegral x/fromIntegral dimx)*(snd xc - fst xc) + fst xc,
                   (fromIntegral y/fromIntegral dimy)*(snd yc - fst yc) + fst yc)

image = generateImage (\x y -> circleToPixel $ bandToCircle $ normalize (x,y)) dimx dimy

main = writePng "band.png" image
