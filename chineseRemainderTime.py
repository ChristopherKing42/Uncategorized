import random

system = """7: Tues(day), Wed(nesday), Thurs(day), Fri(day), Sat(urday), Sun(day), Mon(day)
4: Ear(th), Wat(er), Air, Fir(e)
13: Sub(mill), Lus(eme), Aika(nere), For(iev), Kaud(are), Eust(erin), Te(ezen), Odom(at), Old(atere), Alc(idn), Iker(ia), Gus(lerin), Ota(xere)
23: Fer(mat), Ram(anujan), Arch(imedes), Galo(is), Neu(mann), New(ton), Dir(ichlet), Abel, Euler, Gauss, Wei(erstrass), Brah(magupta), Grot(hendieck), Lag(range), Weyl, Riem(ann), Poin(care), De(scartes), Can(tor), Hilb(ert), Leib(niz), Jac(obi), Euc(lid)
11: Uman(is), Mau(lu), Pand(ita), Pe(pet), Watu(gunung), Men(ala), Sani(scara), Kaj(eng), Lu(ang), Uma, Dadi
9: Harm(ony), Li(berty), Will, Rad(iance), Vir(tue), Intel(lect), Who(leness), Inde(pedence), Memo(ry)
5: Roc, Liz(ar), Spok, Sic(cor), Pyp(ir)"""

cycleNames = ['Week', 'Element', 'Rusqui', 'Man', 'Pawukon', 'Concept', 'Chute']

cycles = system.split('\n')
cycles = [cycle[cycle.index(' ')+1:].split(', ') for cycle in cycles]
cycles = [cycle[n:] + cycle[:n] for cycle in cycles for n in (random.randrange(len(cycle)) if len(cycle) != 7 else 0, )]

with open('cyclic.ics', 'w') as f:
    f.write('BEGIN:VCALENDAR\n')
    f.write('VERSION:2.0\n')
    f.write('PRODID:CHRISTOPHER\n')
    for i in xrange(7):
        n = len(cycles[i])
        for j in xrange(n):
            f.write('BEGIN:VEVENT\n')
            f.write('DTSTART;VALUE=DATE:201901{day:02d}\n'.format(day=j+1))
            f.write('DTEND;VALUE=DATE:201901{day:02d}\n'.format(day=j+1))
            f.write('RRULE:FREQ=DAILY;INTERVAL={n}\n'.format(n=n))
            f.write('SUMMARY:{cycle}: {stage}\n'.format(cycle=cycleNames[i], stage=cycles[i][j]))
            f.write('END:VEVENT\n')
    f.write('END:VCALENDAR\n')
