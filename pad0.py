from PIL import Image
import random

filename = raw_input("File name? ")
img = Image.open(filename, 'r').convert("LA")
scaleFactor = 5
img = img.resize((img.size[0]*scaleFactor, img.size[1]*scaleFactor), Image.ANTIALIAS)
pixels = img.load()

for i in range(img.size[0]):
    for j in range(img.size[1]):
        if random.random() < pixels[i,j][0]/255.0:
            value = 255
        else:
            value = 0
        
        pixels[i,j] = (value, 255)

img.show()
img.convert('RGB').save("binary.bmp")
