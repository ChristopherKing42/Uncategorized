from PIL import Image
import pickle, os, sys

def getColors(im):
    x = 0
    cols = [[] for _ in xrange(im.size[0])]
    for color in im.getdata():
        cols[x].append(color)
        x += 1
        if x % im.size[0] == 0: x = 0
    return cols

def nbrs((x,y), (xsize,ysize)):
    if x > 0: yield (x-1, y)
    if y > 0: yield (x, y-1)
    if x < xsize-1: yield (x+1, y)
    if y < ysize-1: yield (x, y+1)

def getPt(ls, (x,y)): return ls[x][y]
def setPt(ls, (x,y), val): ls[x][y] = val

class Lexer(object):
    def __init__(self, im, colorNames = None):
        size = im.size
        colors = getColors(im)
        comp = [[None for _ in xrange(size[1])] for _ in xrange(size[0])]
        self.graph = []
        self.colorMap = []
        self.pixelMap = []
        n = 0
        for y in xrange(size[1]):
            for x in xrange(size[0]):
                if comp[x][y] != None: continue
                color = colors[x][y]
                isWS = color == (255, 255, 255)
                self.graph.append(set())
                self.colorMap.append(color)
                self.pixelMap.append((x, y))
                toVisit = [(x,y)]
                while toVisit:
                    pt = toVisit.pop()
                    if getPt(comp, pt) != None: continue
                    setPt(comp, pt, n)
                    ncount = 0
                    for nbr in nbrs(pt, size):
                        ncount += 1
                        if color == getPt(colors, nbr): toVisit.append(nbr)
                        else:
                            nbrn = getPt(comp, nbr)
                            if nbrn != None:
                                self.graph[n].add(nbrn)
                                self.graph[nbrn].add(n)
                    assert isWS or ncount == 4
                n += 1
        self.graph = (frozenset(s) for s in self.graph)
        self.graph, self.colorMap, self.pixelMap = tuple(self.graph), tuple(self.colorMap), tuple(self.pixelMap)

        if colorNames == None: colorNames = ColorNames()
        self.cName = colorNames
        self.outside = 0
    
    def cnNode(self, node): return self.cName(self.colorMap[node])

def lexFromFile(fd, backup = True):
    try: useBackup = backup and os.path.getmtime(fd + '~') >= os.path.getmtime(fd)
    except OSError: useBackup = False
    if useBackup:
        with open(fd + '~', 'rb') as backup: return pickle.load(backup)
    else:
        with Image.open(fd) as im: lex = Lexer(im)
        if backup:
            with open(fd + '~', 'wb') as backup: pickle.dump(lex, backup, pickle.HIGHEST_PROTOCOL)
        return lex

class ColorNames(object):
    def __init__(self, rgbPath = 'aek-54.gpl.txt'):
        self.colorNames = {}
        self.colors = []
        with open(rgbPath) as rgbFile:
            while rgbFile.readline() not in ['#\n', '']: pass
            for line in rgbFile:
                vals = line.split()
                r, g, b = vals[:3]
                color = int(r), int(g), int(b)
                self.colors.append(color)
                name = ' '.join(vals[3:-1])
                if color not in self.colorNames: self.colorNames[color] = name
    def __call__(self, color):
        res = self.colorNames.get(color)
        if res == None: res = '#{:02x}{:02x}{:02x}'.format(*color)
        return res
    def savePalette(self, fd = 'palette.txt'):
        with open(fd, 'w') as pFile:
            for color in self.colors:
                pFile.write('ff{:02x}{:02x}{:02x}\n'.format(*color))

def interiors(node, graph, interior, depthMap, depth = 0, par = None):
    depthMap[node] = depth
    jump = depth
    interior[node] = []
    peers = [node]
    for nbr in graph[node]:
        if nbr == par: pass
        elif nbr in depthMap: jump = min(jump, depthMap[nbr])
        else:
            nbrjump, nbrpeers = interiors(nbr, graph, interior, depthMap, depth+1, node)
            jump = min(jump, nbrjump)
            if par == None or nbrjump >= depth + 1: interior[node].append(nbrpeers)
            else: peers.extend(nbrpeers)
    return jump, peers

class AST(object):
    def __init__(self, lex):
        self.lex = lex
        
        ints = {}
        interiors(self.lex.outside, self.lex.graph, ints, {})

        snodes = [] #sphaghet nodes
        calls = [] #calls to painticates
        paintCls = {} #painticates
        for (node, inside) in ints.iteritems():
            color = self.lex.colorMap[node]
            if node == self.lex.outside: components = inside
            elif color in ((128,128,128), (255, 255, 255)): pass
            elif len(inside) == 0: snodes.append(node)
            elif len(inside) == 1 and len(inside[0]) == 1:
                inside = inside[0][0]
                if self.lex.colorMap[node] not in ((128,128,128), (255, 255, 255)):
                    if self.lex.colorMap[inside] == (255, 255, 255):
                        assert not ints[inside]
                        calls.append(node)
                    elif self.lex.colorMap[inside] == (128, 128, 128):
                        if len(ints[inside]) == 0:
                            paintCls.setdefault(color, [])
                            paintCls[color].append((node, None))
                        elif len(ints[inside]) == 1 and len(ints[inside][0]) == 1:
                            assert not ints[ints[inside][0][0]]
                            paintCls.setdefault(color, [])
                            paintCls[color].append((node, self.lex.colorMap[ints[inside][0][0]]))
                    else: assert False
            else: assert False

        self.paints = []
        for (color, nodes) in paintCls.iteritems():
            clauses = []
            for (node, center) in nodes:
                head = []
                body = []
                comp = next(c for c in components if node in c)
                for node_ in comp:
                    if node_ in snodes:
                        body.extend((node_, s) for s in self.lex.graph[node_] if s < node_ and s in snodes)
                    elif node_ in calls:
                        assert not any(True for n in self.lex.graph[node_] if n == node or n in calls)
                        body.append(Stmt(self.lex.colorMap[node_], self.lex.cName, {self.lex.colorMap[s]: s for s in self.lex.graph[node_] if s in snodes}))
                    elif node_ == node:
                        head.extend((self.lex.colorMap[s], s) for s in self.lex.graph[node] if s in snodes)
                    else: assert self.lex.colorMap[node_] not in paintCls
                clauses.append((head, body, self.lex.pixelMap[node]))
                if center != None:
                    args = {}
                    for (color_, var) in head: args[color_] = var
                    head.append((center, Struct(color, self.lex.cName, args)))
            self.paints.append(Painticate(color, self.lex.cName, clauses))
        for p in self.paints:
            print p
            print

def astFromFile(fd, backup = True): return AST(lexFromFile(fd, backup))

class Painticate(object):
    def __init__(self, ident, cn, clauses):
        self.ident = ident
        self.cn = cn
        self.clauses = tuple(clauses)
        head, _, _ = clauses[0]
        args = frozenset([c for (c,v) in head])
        struct = next((v for (c,v) in head if isinstance(v, Struct)), None)
        for (head, _, _) in clauses:
            assert args == frozenset([c for (c,v) in head])
            assert struct == next((v for (c,v) in head if isinstance(v, Struct)), None)
    def __repr__(self):
        clsReps = []
        for (head, body, pixel) in self.clauses:
            clsReps.append("Head: {head}, Body: {body}, Pixel: {pixel}".format(head=[(self.cn(c), v) for (c, v) in head], body=body, pixel=pixel))
        return "Painticate({cn}, {clauses})".format(cn=self.cn(self.ident), clauses='; '.join(clsReps))

class Stmt(object):
    def __init__(self, ident, cn, args):
        self.ident = ident
        self.cn = cn
        self.args = args
    def __repr__(self):
        argReps = []
        for (color, var) in self.args.iteritems():
            argReps.append("{color}:{var}".format(color=self.cn(color), var=var))
        return "Stmt({cn}, {args})".format(cn=self.cn(self.ident), args=', '.join(argReps))

class Struct(object):
    def __init__(self, ident, cn, args):
        self.ident = ident
        self.cn = cn
        self.args = args
    def __repr__(self):
        argReps = []
        for (color, var) in self.args.iteritems():
            argReps.append("{color}:{var}".format(color=self.cn(color), var=var))
        return "Struct({cn}, {args})".format(cn=self.cn(self.ident), args=', '.join(argReps))

res = astFromFile(sys.argv[1])
