import Data.Array
import Data.List (nub)

newtype GL32 = GL32 (Array (Int,Int) Int) deriving (Eq, Ord)

instance Show GL32 where
    show (GL32 arr) = unlines [unwords [show $ arr ! (x,y)|x<-[0..2]]|y<-[0..2]]

ident = GL32 $ listArray ((0,0),(2,2)) [1,0,0,0,1,0,0,0,1]
mult (GL32 arr1) (GL32 arr2) = GL32 $ array ((0,0),(2,2)) [((x,y),ele (x,y)) | x <- [0..2], y <- [0..2]] where
    ele (x,y) = (sum $ map (\k -> arr1 ! (x,k) *  arr2 ! (k,y)) [0..2]) `mod` 2

allMatrices = map (GL32 . listArray ((0,0),(2,2))) $ lists 9 where
    lists 0 = [[]]
    lists n = (map (0:) $ lists (n-1)) ++ (map (1:) $ lists (n-1))

order2 = filter (\m -> m `mult` m == ident) allMatrices
order7 = filter (\m -> iterate (`mult` m) ident !! 7 == ident) allMatrices

pairs = filter (\(m1,m2) -> let m = (m1 `mult` m2) in m `mult` m `mult` m == ident) [(m1,m2) | m1 <- order2, m2 <- order7]

generated (m1,m2) = igo generations where
    generations = iterate (\l -> nub $ l >>= \m -> [m, m `mult` m1, m `mult` m2]) [ident]
    igo (x1:x2:xs) = if length x1 == length x2 then x1 else igo (x2:xs)
