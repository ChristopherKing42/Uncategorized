tree=[] #Contains lists for each depth

class node:
    def __init__(self,state): #Takes a hypothetical state as an arguement
        self.parent=None
        self.children=[]
        self.state=state
        self.value=0
        self.turn=None
    def pass_val(self): #Passes node's current value up the tree
        if self.parent:
            self.parent.value=self.value

def add_node(node_arg, node_parent, depth): #Takes the node to add and its parent
    node_arg.parent=node_parent
    if node_parent:
        node_parent.children.append(node_arg)
    try:
        tree[depth].append(node_arg)
    except(IndexError):
        tree.append([])
        tree[depth].append(node_arg)

def pass_values(parent, turn, num_players): #passes the values up the tree. 
    for child in parent.children:
        if child.children:
            if turn == 0:
                pass_values(child, turn+(num_players-1), num_players)
            else:
                pass_values(child, turn-1, num_players)
    if turn: #If the opponent's turn, minimize their chances
        parent.value=min(parent.children, key=lambda x:x.value).value
        parent.turn=True
    else:
        parent.value=max(parent.children, key=lambda x:x.value).value
        parent.turn=False
