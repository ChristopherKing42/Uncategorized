def unitRoots(c):
    if c:
        head = c[0]
        tail = c[1:]
        for perm in unitRoots(tail): yield perm
        i = 0
        for ele in tail:
            for perm in unitRoots(tail[:i] + tail[i+1:]):
                yield ((head, ele),) + perm
            i += 1
    else:
        yield ()

urs = {}
for i in xrange(11):
    urs[i] = tuple(unitRoots(range(i)))

def execPerm(s, p):
    s = list(s)
    for (ele1, ele2) in p:
        s[ele1], s[ele2] = s[ele2], s[ele1]
    s = ''.join(s)
    return s

def main():
    with open('/home/theking/Downloads/yob2010.txt') as nameFile:
        names = {}
        for line in nameFile:
            name, gender, count = line.split(',')
            count = int(count)
            if count < 100: continue
            if len(name) > 10: continue #Too many permutations to check.
            name = name.upper()
            names[name] = gender, count
        twins = []
        for (name1, (gender1, count1)) in names.items():
            base = name1[::-1]
            for perm in urs[len(name1)]:
                name2 = execPerm(name1, perm)
                if name1 >= name2: continue
                val = names.get(name2)
                if val == None: continue
                gender2, count2 = val
                twins.append((name1, gender1, count1, name2, gender2, count2))
        
        twins = sorted(set(twins), key=lambda (_n1,_g1,c1,_n2,_g2,c2): min(c1, c2), reverse=True)
        print twins[:10]

main()
