from fractions import gcd

def lcm(a, b): return a*b//gcd(a, b)
def lcms(*l):
    res = 1
    for n in l:
        res = lcm(res, n)
    return res

mem = {}

def partHelper(n, upper):
    if n == 0: yield ()
    elif n < 0: pass
    elif upper == 1: yield () #We omit 1s
    else:
        for t in xrange(1, upper+1):
            for p in partHelper(n - t, t):
                yield (t, ) + p
        

def partitions(n): return partHelper(n, n)

def lambdau(n, start = (), debug = False):
    startLcm = lcms(*start)
    bestRes = 0
    bestP = (n, 0)
    for p in partitions(n - sum(start)):
        res = lcms(startLcm, *p)
        if res > bestRes:
            bestRes = res
            bestP = p
            if debug: print bestRes, p
    return bestRes, start + bestP + (1,) * (n - sum(start + bestP))

wheelSizes = (7, 4, 13, 9, 23, 11, 5)
capcity = reduce(lambda x,y: x*y, wheelSizes, 1)

def modular(n, m): return (n + m/2) % m - m/2 #n modular m, except its negative if close to m. For example modular(6,7) is -1.
def daysToRots(days):
    rots = ()
    for i in xrange(7):
        rots += (modular(days, wheelSizes[i]), )
    return rots

def rotsToDays(rots):
    mod = 1 #The current modulus of the past solutions. (Initialized to 1 since we haven't solved anything yet.)
    sol = 0
    for i in xrange(7):
        if rots[i] == None: continue #The user can omit a wheel if they do not know how much it rotated.
        while (sol - rots[i]) % wheelSizes[i] != 0: #We essentially use bruteforce to find a solution for the ith wheel
            sol += mod #n == 0 (mod n), so adding mod to sol does disrupt previous solutions
        mod *= wheelSizes[i] #Multiply mod by the wheelsize so we do not disrupt it in the future
    return modular(sol, mod) #We technically only know the solution modular mod, so there is some ambiguity. If you use all the wheels, mod days will be over 10,000 years, so its no a big issue.

#examples
print daysToRots(74) #Outputs (-3, -2, -4, 2, 5, -3, -1), which means that the first wheel will be rotated 3 symbols left, etc...
print rotsToDays((-3, -2, -4, 2, 5, -3, -1)) #Outputs 74, which means that if you rotate each of the wheels by the amounts specified, your date will be 74 days in the future. Useful for finding the time difference between two dates.
