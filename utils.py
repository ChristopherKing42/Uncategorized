import json
import sys
def mal(): print "Sorry, the input was malformed. Try again."

print "BEGIN PROGRAM\n"

print "Hello, today I am going to determine your Von Neumann-Morgenstern utility function by asking you a few simple questions. First as an ice breaker, please enter you name.\n"
name = raw_input("NAME: ")
print

print "Good. Now for some similarly casual questions. Please list some possible outcomes of your life (in no particular order). The utility function will only be calculated for these outcomes. (Enter the empty string when you are done.)\n"
outcomes = []
nOut = 0
while True:
    outcome = raw_input("OUTCOME #{nOut}: ".format(nOut=nOut))
    if not outcome:
        if nOut == 0:
            print "You must input at least one outcome."
            continue
        else: break
    outcomes.append(outcome)
    nOut += 1
print

utils = [None] * nOut
while True:
    care = raw_input("Do you care which outcome you experince?(y/n) ")
    care = care[0].lower()
    if care == 'y':
        print
        print "Please enter the number for the best possible outcome of your life. (In the event of a tie, chose the one that comes first in alphabetical order.)"
        while True:
            bestStr = raw_input("BEST FATE: ")
            try:
                best = int(bestStr)
                assert (0 <= best and best < nOut)
            except ValueError, AssertionError: pass
            else: break
            mal()
        print

        print "Now do the same for the worst outcome of your life."
        while True:
            worstStr = raw_input("WORST FATE: ")
            try:
                worst = int(worstStr)
                assert (0 <= worst and worst < nOut)
            except ValueError, AssertionError:
                print repr(worstStr)
                print worst
            else:
                if best == worst:
                    print
                    print "You said the following:"
                    print "\t- {best} is a best possible outcome.".format(best=repr(outcomes[best]))
                    print "\t- {worst} is a worst possible outcome.".format(worst=repr(outcomes[worst]))
                    print "\t- You care about what outcome you experince."
                    print "This is a contradiction. You have lied to me. Goodbye."
                    sys.exit(-1)
                else:
                    break
            mal()
        print

        utils[best] = 100.0
        utils[worst] = 0.0
        print "Thank you. Now for some more questions.\n"
        for i in xrange(nOut):
            if i in [best, worst]: continue
            start = -0.05
            for step in [10.0, 1.0, 0.1]:
                width1 = len(str(start+10*step))
                width2 = len(str(100-start-step))
                print "Consider the ten following scenarios."
                for j in xrange(10):
                    print '\t{j}) You have a {good: >{width1}}% chance of {best} and a {bad: >{width2}}% chance of {worst}.'.format(j=j, good=start+(j+1)*step, width1=width1, best=repr(outcomes[best]), bad=100-start-(j+1)*step, width2=width2, worst=repr(outcomes[worst]))
                print
                print 'What is the first scenario that is better than the scenario that guarantees {outcome}?'.format(outcome=repr(outcomes[i]))
                if step == 10:
                    print '(If none are, leave the input blank.)'
                while True:
                    scen = raw_input("SCENARIO: ")
                    if step == 10 and scen == '':
                        scen = 10
                        break
                    if scen in "0123456789":
                        scen = int(scen)
                        break
                    else: mal()
                print
                start += scen*step
                if scen == 10: break
            utils[i] = start + 0.05
        break
    elif care == 'n':
        print
        print "Interesting...\n"
        for outcome in outcomes:
            utils[outcome] = 0.0
        break
    else: mal()

print "Done! That question was the last question. We can now calculate your Von Neumann-Morgenstern utility function. (Disclaimer: This will function will be correct if (1) your answered the above questions honestly and (2) you are a VNM-rational agent. Additionally, the utility function will only be unique up to a additivie and positive multiplicative constant.) We will denote your utility function as u_{{{name}}}.\n".format(name=name)

for i in xrange(nOut):
    print 'u_{{{name}}}({outcome}) = ~{util} utils'.format(name=name, outcome=repr(outcomes[i]), util=utils[i])
print

print "Now making any choice will be simple. Choose the option x that maximizes (\\sum_{{y in outcomes}}(P_x(y)u_{{{name}}}(y))), where P_x(y) is the probability of experincing y after choosing x.\n".format(name=name)
print "Thank you. Feel free to use the program if you change your mind about anything or wish to include more detailed outcomes. I hope this was an enjoyable experince.\n"

with open("{name}.util".format(name=name), 'w') as f:
    json.dump({outcomes[i]:utils[i] for i in xrange(nOut)}, f)

print "For you convience, your utility function has been saved to {name}.util.\n".format(name=name)
print "END PROGRAM"
